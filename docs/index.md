---
author: Pierre
title: acceuil
---


## Sommaire 


### VIVE LA FORGE DU LIBRE EDUCATIF.

Vous êtes actuellement sur le site de Monsieur Munch : Vous pourrez trouver ici diverses ressources : feuilles de calcul mental, feuilles d'exercices, fiche de cours, etc...


## NEPAS LIRE LA SUITE : Work in progress

Vous trouverez un lien vers le [dépôt](https://forge.apps.education.fr/modeles-projets/mkdocs-simple-review/){ .md-button target="_blank" rel="noopener" } et un lien vers un [tutoriel](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/){ .md-button target="_blank" rel="noopener" }.

Ce modèle se veut épuré mais présente les principales fonctionnalités pour vous permettre une appropriation rapide.
On trouvera :

- "Chapitre 1" avec un seul fichier, le plus simple possible, avec trois exemples d'admonitions (voir le tutoriel)

- "Gros chapitre" contenant plusieurs fichiers, correspondant à des parties du chapitre.  

    - Dans Chapitre 2 - la Page 1 vous trouverez un exemple d'admonition d'exercice avec la soution cliquable
    - Dans Chapitre 2 - la Page 2 vous trouverez un exemple avec une image
    - Dans Chapitre 2 - Compléments vous trouverez un exemple de QCM

    Ce chapitre est organisé grâce à un fichier `.pages`

- "Crédits."

😊 Il existe beaucoup d'autres possibilités pour votre site que vous découvrirez dans le tutoriel.

👉 Vous pouvez désormais modifier le contenu des fichiers pour créer votre propre site web.

## Accueil à personnaliser une fois votre propre site réalisé

😂 Ne pas oublier de personnaliser cette page qui sera l'accueil de votre propre site.

Voir le tutoriel [Personnaliser l'accueil](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/01_demarrage/1_demarrage/#iv-personnaliser-la-page-daccueil-du-site-que-vous-avez-clone){:target="_blank" }

👉 Exemple d'accueil : 

# Bienvenue dans le cours de ...

Dans ce cours, nous traiterons de ...


[^1]: Voir le tutoriel : [tutoriel pour faire un fork](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/08_tuto_fork/1_fork_projet/){:target="_blank" }

